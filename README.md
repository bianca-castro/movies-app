# Getting Started 

To start using this project clone this repo in your local.

Executes the following command to install dependencies:

### `npm i`

Once it is finished, executes the following command to launch it.

### `npm start`

Automatically it will be open in /localhost:3000 in a new browser window.
