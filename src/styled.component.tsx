import styled from 'styled-components'


export const MovieListContainer = styled.div`
margin: 10px;
display:flex;
flex-direction: row;
overflow: auto;
    width: 100vw;
    height: 450px;
    @media (max-width: 600px) {
        flex-direction: column;
        height: auto;
        align-items: center;
      }`;
