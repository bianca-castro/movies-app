
import { useState, useEffect } from "react"
import MovieList from "./components/List";
import { MovieListContainer } from "./styled.component.tsx"
import Header from "./components/Header"

function App() {
  const [movies, setMovies] = useState([]);
  const getMovieList = async () => {
    const response = await (await fetch("https://test-data-interviews.s3.eu-west-1.amazonaws.com/accedoTest.json")).json();
    setMovies(response)
  }
  useEffect(() => {
    getMovieList()
  }, [])
  return (
    <>
      <Header />
      <MovieListContainer >
        <MovieList movies={movies} />
      </MovieListContainer>
    </>
  );
}

export default App;
