
import { useEffect, useState, } from "react";
import { Card, Name, Image, Tooltip, } from "./styled.component.tsx"
import ReactPlayer from 'react-player'

const MovieList = (props) => {

    const [showTooltip, setShowTooltip] = useState(true);
    const [reproduce, setReproduce] = useState();
    const [watchedList, setWatchedList] = useState([]);

    const handleClicked = (movie) => {
        let newFavorites = watchedList
        if (watchedList.indexOf(movie) === -1) {
            watchedList.push(movie);
            setWatchedList(newFavorites);
            window.localStorage.setItem("favorites", JSON.stringify(watchedList));
        }
    }
    useEffect(() => {
        const items = JSON.parse(localStorage.getItem('favorites'));
        if (items) {
            setWatchedList(items);
        }
    }, []);
    const reproduceMovie = (movie) => {
        return (<><ReactPlayer
            url={movie}
            width='100%'
            height='100%'
            controls={true}
            playing
            z-index="1000"
            onEnded={() => setReproduce(null)}
            style={{ position: "absolute", top: "0", left: "0", background: "#e9e6e6" }}
        /></>)
    }
    return (< >{props.movies.map((movie, index) => <Card key={movie.id}>
        <Image data-tip={movie.summary} onMouseOver={() => { setShowTooltip(index) }} onMouseOut={() => setShowTooltip(false)} src={movie.image} alt="movie" onClick={() => { setReproduce(index); handleClicked(movie, index) }}></Image>
        <Name>{movie.name}</Name>
        {reproduce === index && reproduceMovie(movie.mediaUrl)}
        {showTooltip === index && <Tooltip>{movie.summary}</Tooltip>}
    </Card>)}
    </>)
}

export default MovieList