import styled from 'styled-components'


export const Card = styled.div`
border: 1px solid #8d8d9d;
    margin: 2px;
    border-radius: 7px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    &:hover{
        border:2px solid #1e1e1e;
        transition: transform .2s;
        padding:4px;
    }
    @media (max-width: 600px) {
        width: 220px;
      }`

export const Tooltip = styled.span`
border-bottom:1px dashed #000; 
width:400px;
border-radius:10px;
background:#000;
color: #fff;
text-align:center;
position:absolute;
z-index:10;
top:100px;
`

export const Wrapper = styled.div`
max-width: 50px !important;
border: 2px solid blue`

export const Image = styled.img`
padding: 5px;
cursor:pointer;
&:hover{
    transform: scale(1.05);
}`

export const Name = styled.div`
text-align: center`

export const Navbar = styled.div`
background-color:white;
display: flex;
justify-content: space-between;
height: 30px;
align-items: center;
padding: 10px;
`
export const NavbarWatchedSection = styled.div`
background-color:white;
display: flex;
justify-content: space-between;
height: 30px;
align-items: center;
padding: 10px;
`
