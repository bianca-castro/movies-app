import { NavbarWatchedSection, Name, Card, Image } from "./styled.component.tsx"
import { MovieListContainer } from "../styled.component.tsx"
import { Link } from 'react-router-dom';
import { useState } from "react";

const Watched = () => {
    const [favorites] = useState(() => {
        if (localStorage.getItem("favorites") !== null) {
            const savedItems = localStorage.getItem("favorites");
            const initialValue = JSON.parse(savedItems);
            return initialValue.filter((value, index, self) => index === self.findIndex((t) => (
                t.place === value.place && t.name === value.name
            )))
        }
        return null;
    });
    return (<>
        <NavbarWatchedSection><span>Watched list</span> <Link to="/"
        >Go back home</Link></NavbarWatchedSection>
        <MovieListContainer>{favorites === null ? "No items as favorite" :
            favorites.map(movie => {
                return <> <Card key={movie.id}>
                    <Image src={movie.image} alt="movie" ></Image>
                    <Name>{movie.name}</Name>
                </Card></>
            })}</MovieListContainer>
    </>)
}
export default Watched