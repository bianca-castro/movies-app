import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { createGlobalStyle } from 'styled-components'

import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Watched from './components/Watched';

const GlobalStyle = createGlobalStyle`
  body {
   background-color: #c9c8c8;
   margin: 0;
   font-family: Arial, Helvetica, sans-serif;
  }`
ReactDOM.render(
  <React.StrictMode>
    <GlobalStyle />
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="/navigate" element={<Watched />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
